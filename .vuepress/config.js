module.exports = {
  title: 'PDS Lab',
  description: 'Assignments, Solutions and much more...',
  markdown: {
    lineNumbers: true
  },
  themeConfig: {
    sidebar: 'assignments/',
    nav: [
      { text: 'Tips', link: '/tips/' },
      { text: 'Assignments', link: '/assignments/' }
    ]
  },
  base: '/blog/',
  dest: 'public',
  plugins: [
    [
      'vuepress-plugin-code-copy', true
    ],
    [
      'vuepress-plugin-mathjax',
      {
        target: 'svg',
        macros: {
          '*': '\\times',
        },
      },
    ]
  ]
}