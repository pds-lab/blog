#include <stdio.h>
#include <stdlib.h>

#define MAXSIZE 100
#define MAX 1000

void print_array(int A[MAXSIZE], int size) {
  int i;
  for (i = 0; i < size; i++) {
    printf("%d ", A[i]);
  }
  printf("\n\n");
}

int is_prime(int n) {
  int i;
  if (n < 2)
    return 0;
  for (i = 2; i * i <= n; i += 1) {
    if (n % i == 0)
      return 0;
  }
  return 1;
}

int main() {
  int n, i, new_size = 0;
  int A[MAXSIZE];
  int temp[MAXSIZE];
  scanf("%d", &n);
  if (n > MAXSIZE) {
    printf("Maximum size of the array can be %d.\n", MAXSIZE);
    return -1;
  }
  if (n < 0) {
    printf("Minimum size of the array can be 0.\n");
    return -1;
  }
  printf("An array of %d random numbers between 0 and %d:\n", n, MAX);
  for (i = 0; i < n; i++) {
    A[i] = rand() % MAX;
  }
  print_array(A, n);
  for (i = 0; i < n; i++) {
    if (!is_prime(A[i])) {
      temp[new_size] = A[i];
      new_size++;
    }
  }

  printf("The array after removing prime numbers:\n");
  for (i = 0; i < new_size; i++) {
    A[i] = temp[i];
  }
  n = new_size;
  print_array(A, n);

  return 0;
}