#include <math.h>
#include <stdio.h>
#include <stdlib.h>

double fun1(double x) { return pow(x, 3) - x - 11; }

int cbracket(double a, double b) { return (fun1(a) * fun1(b) <= 0) ? 1 : 0; }

double rootb(double a, double b, double eps, int Nmax) {
  if (cbracket(a, b) == 0) {
    // A root might exist even if f(a) * f(b) > 0 if the function is complex
    // But we can't compute it using bisection-method.
    printf("Can't compute any root of f(x) between %lf and %lf\n", a, b);
    exit(0);
  }
  // If f(a) * f(b) <= 0, we can find the root using bisection method.
  int i;
  double fa, fb, mid, fmid;
  if (fun1(a) == 0)
    return a;
  if (fun1(b) == 0)
    return b;
  for (i = 1; i <= Nmax; i++) {
    mid = (a + b) / 2;
    fa = fun1(a);
    fb = fun1(b);
    fmid = fun1(mid);
    if ((fmid == 0) || (fb - fa < eps))
      return mid;
    if (fa * fmid < 0)
      b = mid;
    else // equivalent to : else if (fb * fmid < 0)
      a = mid;
  }
  printf("A root exists between %lf and %lf. ", a, b);
  printf("Increase Nmax or eps to get the root.\n");
  exit(0);
}

int main() {
  double a, b, eps, rootval;
  int Nmax;
  scanf("%lf %lf %lf %d", &a, &b, &eps, &Nmax);
  rootval = rootb(a, b, eps, Nmax);
  printf("A root of f(x) between %lf and %lf is %lf\n", a, b, rootval);
  return 0;
}
