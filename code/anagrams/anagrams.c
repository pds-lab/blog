#include <stdio.h>
#include <string.h>

#define MAXC 15
#define MAXW 100
typedef struct {
  char word[MAXC];
  char sorted[MAXC];
} word_t;

int readwords(word_t wordlist[]) {
  // printf("Enter the number of words.\n");
  int n, i;
  scanf("%d%*c", &n);
  for (i = 0; i < n; i++) {
    scanf("%s", wordlist[i].word);
  }
  return n;
}

void sortword(word_t *pword) {
  int i, j, size = strlen(pword->word);
  char tmp_wd[MAXC], tmp_c;
  strcpy(tmp_wd, pword->word);
  for (i = size - 1; i > 0; i--) {
    for (j = 0; j < i; j++) {
      if (tmp_wd[i] > tmp_wd[j]) {
        tmp_c = tmp_wd[i];
        tmp_wd[i] = tmp_wd[j];
        tmp_wd[j] = tmp_c;
      }
    }
  }
  strcpy(pword->sorted, tmp_wd);
}

int is_anagram(word_t *pword1, word_t *pword2) {
  return !strcmp(pword1->sorted, pword2->sorted);
}

void sortwordlist(word_t wordlist[], int numwords) {
  int i, j;
  word_t tmp_wd_t;
  for (i = numwords - 1; i > 0; i--) {
    for (j = 0; j < i; j++) {
      if (strcmp(wordlist[j].sorted, wordlist[j + 1].sorted) > 0) {
        tmp_wd_t = wordlist[j];
        wordlist[j] = wordlist[j + 1];
        wordlist[j + 1] = tmp_wd_t;
      }
    }
  }
}

void printanagrams(word_t wordlist[], int numwords) {
  int i, j;
  sortwordlist(wordlist, numwords);
  if (numwords > 0)
    printf("%s", wordlist[0].word);
  for (i = 1; i < numwords; i++) {
    if (!is_anagram(wordlist + i, wordlist + i - 1))
      printf("\n%s", wordlist[i].word);
    else
      printf(" %s", wordlist[i].word);
  }
  printf("\n");
}

int main() {
  word_t wordlist[MAXW];
  int i, numwords;
  numwords = readwords(wordlist);
  for (i = 0; i < numwords; i++)
    sortword(wordlist + i);
  printanagrams(wordlist, numwords);
}