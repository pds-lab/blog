#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 4

// Modified from 19MT10017's submission
typedef struct fifo {
  char d[MAX][11];
  int n;
  int front;
  int rear;
} queue;

void init(queue *q) {
  q->front = -1;
  q->rear = -1;
  q->n = 0;
}

int isfull(queue *q) { return (q->n == MAX); }

int isempty(queue *q) { return (q->front == -1); }

int add(queue *q, char *str) {
  if (isfull(q))
    return -1;

  else {
    if (q->front == -1) {
      q->front = 0;
      q->rear = 0;
    } else {
      q->rear = (q->rear + 1) % MAX;
    }
    strcpy(q->d[q->rear], str);
    q->n++;
    return 0;
  }
}

int del(queue *q) {
  if (isempty(q))
    return -1;
  else {
    int f = q->front;
    if (q->front == q->rear) {
      q->front = -1;
      q->rear = -1;
    } else
      q->front = (q->front + 1) % MAX;
    q->n--;
    return f;
  }
}

void print(queue *q) {
  if (isempty(q))
    printf("The queue is empty!!!\n");
  else {
    int i;
    printf("%s  ", q->d[q->front]);
    for (i = q->front + 1; i != (q->rear + 1) % MAX; i = (i + 1) % MAX) {
      printf("%s  ", q->d[i]);
    }
    printf("\n");
  }
}

int main() {
  char c[10], s[11];
  int err;
  queue q;
  init(&q);
  printf("MENU:\n");
  printf("Add : Read string and add it to queue\n");
  printf("Delete : Delete front element from the queue and print it\n");
  printf("Print : Print the elements of the queue\n");
  printf("Quit : Exit\n");
  printf("Enter your choice:\n");
  scanf("%s", c);
  while (strcmp(c, "Quit") != 0) {
    if (strcmp(c, "Add") == 0) {
      scanf("%s", s);
      err = add(&q, s);
      if (err == -1)
        printf("Could not add %s. Queue is full!!!\n", s);
      else
        printf("Added %s succesfully\n", s);
    } else if (strcmp(c, "Delete") == 0) {
      err = del(&q);
      if (err == -1)
        printf("Could not delete. Queue is empty!!!\n");
      else
        printf("Deleted %s succesfully\n", q.d[err]);
    } else if (strcmp(c, "Print") == 0)
      print(&q);
    else
      printf("Command not recognised\n");
    scanf("%s", c);
  }
  return 0;
}
