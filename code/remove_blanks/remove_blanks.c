#include <ctype.h>
#include <stdio.h>
#include <string.h>
#define MAXSIZE 1000

int removeblanks(char sentence[MAXSIZE]) {
  int i, newsize = 0, num_words = 0, size = strlen(sentence);
  sentence[size] = ' ';
  char new_sentence[MAXSIZE];
  size++;
  sentence[size] = '\0'; // or sentence[size + 1] = 0;
  for (i = 0; i < size; i++) {
    if (!isspace(sentence[i])) {
      new_sentence[newsize] = sentence[i];
      newsize++;
    }
    if ((isspace(sentence[i])) && (i > 0) && (!isspace(sentence[i - 1])))
      num_words++;
  }
  new_sentence[newsize] = 0;
  printf("New sentence:\n%s\n", new_sentence);
  return num_words;
}

int main() {
  char sentence[MAXSIZE];
  int num_words;
  scanf("%[^\n]%*c", sentence);
  num_words = removeblanks(sentence);
  printf("Number of words = %d.\n", num_words);
  return 0;
}