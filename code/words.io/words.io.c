#include <ctype.h>
#include <stdio.h>
#include <string.h>

#define MAX_WORDSIZE 100
#define MAX_WORDS 500

int main(int argc, char **argv) {
  // Take input file path from either command line or stdin
  if (argc < 2) scanf("%[^\n]%*c", argv[1]);
  freopen(argv[1], "r", stdin);

  char ch;
  char words[MAX_WORDS][MAX_WORDSIZE], temp_word[MAX_WORDSIZE];
  int word_size = 0, num_words = 0, num_char = 0, i_words, j_words;
  while ((ch = getchar()) != EOF) {
    num_char++;
    if (isalpha(ch)) {
      words[num_words][word_size] = ch;
      word_size++;
    } else if (word_size) {
      words[num_words][word_size] = 0;
      num_words++;
      word_size = 0;
    }
  }
  if (word_size) {
    words[num_words][word_size] = 0;
    num_words++;
    word_size = 0;
  }
  // Works like Selection sort
  for (i_words = 0; i_words < num_words; i_words++) {
    for (j_words = i_words + 1; j_words < num_words; j_words++) {
      if (strcmp(words[i_words], words[j_words]) > 0) {
        strcpy(temp_word, words[i_words]);
        strcpy(words[i_words], words[j_words]);
        strcpy(words[j_words], temp_word);
      }
    }
  }
  printf("Number of characters = %d\n", num_char);
  printf("Number of words = %d\n", num_words);
  // Take output file path from either command line or stdin
  if (argc < 2) scanf("%[^\n]%*c", argv[2]);
  freopen(argv[2], "w+", stdout);

  printf("Distinct words:\n");
  if (num_words) printf("\t%s\n", words[0]);
  for (i_words = 1; i_words < num_words; i_words++) {
    if (strcmp(words[i_words], words[i_words - 1]))
      printf("\t%s\n", words[i_words]);
  }
  return 0;
}