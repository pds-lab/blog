#include <math.h>
#include <stdio.h>

double exp_rec(double x, int curr_term, int max_term) {
  double res = x / curr_term;
  if (curr_term > max_term)
    return 0;
  return res + res * exp_rec(x, curr_term + 1, max_term);
}

int main() {
  int n;
  double x;
  scanf("%lf %d", &x, &n);
  double e_x = 1 + exp_rec(x, 1, n);
  printf("exp(%lf) from Taylor Series till %d terms = %lf.\n", x, n, e_x);
  return 0;
}