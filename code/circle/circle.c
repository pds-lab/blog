#include <math.h>
#include <stdio.h>

double distance(double x_i, double y_i, double x_j, double y_j) {
  return pow(pow(x_j - x_i, 2) + pow(y_j - y_i, 2), 0.5);
}

// returns (x_i^2 + y_i^2) - (x_j^2 + y_j^2)
double square_diff(x_i, y_i, x_j, y_j) {
  return (pow(x_i, 2) + pow(y_i, 2)) - (pow(x_j, 2) + pow(y_j, 2));
}

int main() {
  double x1, y1, x2, y2, x3, y3, x4, y4, d12, d23, d31, d4, r, res = 10e-5;
  double denom, k, s, num_k, num_s;
  scanf("%lf %lf %lf %lf %lf %lf", &x1, &y1, &x2, &y2, &x3, &y3);
  scanf("%lf %lf", &x4, &y4);
  d12 = distance(x1, y1, x2, y2);
  d23 = distance(x2, y2, x3, y3);
  d31 = distance(x3, y3, x1, y1);
  printf("Length of AB = %lf, BC = %lf, CA = %lf\n", d12, d23, d31);

  // Finding the circumcenter first
  denom = 2 * ((x1 - x2) * (y1 - y3) - (x1 - x3) * (y1 - y2));
  num_k = (y1 - y3) * square_diff(x1, y1, x2, y2) -
          (y1 - y2) * square_diff(x1, y1, x3, y3);
  num_s = (x1 - x2) * square_diff(x1, y1, x3, y3) -
          (x1 - x3) * square_diff(x1, y1, x2, y2);
  k = num_k / denom;
  s = num_s / denom;
  r = distance(k, s, x1, y1);
  printf("Circumcircle of triangle ABC:\n\t+++ Radius = %lf.\n", r);
  printf("\t+++ Coordinates = (%lf, %lf).\n", k, s);
  d4 = distance(k, s, x4, y4);
  printf("(%lf, %lf) lies ", x4, y4);
  if (fabs(r - d4) < res) {
    printf("on the circle.\n");
  } else if (d4 < r) {
    printf("inside the circle.\n");
  } else {
    printf("outside the circle.\n");
  }
  return 0;
}