#include <math.h>
#include <stdio.h>
#define MAXSIZE 1000

double distance(double x1, double y1, double x2, double y2) {
  return pow(pow(x2 - x1, 2) + pow(y2 - y1, 2), 0.5);
}

int main() {
  int size, i, j, min_i, min_j;
  double min_dist, curr_dist;
  double x[MAXSIZE], y[MAXSIZE];
  scanf("%d", &size);
  for (i = 0; i < size; i++) {
    scanf("%lf %lf", &x[i], &y[i]);
  }
  if (size < 2) {
    printf("There should be atleast 2 points for finding distance.\n");
    return -1;
  }
  min_i = 0;
  min_j = 1;
  min_dist = distance(x[min_i], y[min_i], x[min_j], y[min_j]);
  for (i = 0; i < size; i++) {
    for (j = i + 1; j < size; j++) {
      curr_dist = distance(x[i], y[i], x[j], y[j]);
      if (curr_dist < min_dist) {
        min_i = i;
        min_j = j;
        min_dist = curr_dist;
      }
    }
  }
  printf("Points %d (%lf, %lf) and %d (%lf, %lf) have the minimum distance of "
         "%lf between them.\n",
         min_i, x[min_i], y[min_i], min_j, x[min_j], y[min_j], min_dist);
  return 0;
}