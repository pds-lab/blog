#include <math.h>
#include <stdio.h>
#define PI 3.141593

int main() {
  int num_inv, inum, deno;
  double res, cos, step, theta, term;
  scanf("%d", &num_inv);
  scanf("%lf", &res);
  if (res >= 0.001)
    return -1;
  step = (2 * PI) / num_inv;
  // * Loop for all the points
  for (inum = 0; inum < num_inv + 1; inum++) {
    theta = inum * step;
    // * Calculate cos(theta)
    cos = term = 1;
    for (deno = 2;; deno += 2) {
      term = -1 * term * (theta * theta) / (deno * (deno - 1));
      // * Break the loop once "absolute value of the next term" < "res"
      if (fabs(term) < res)
        break;
      cos += term;
    }
    printf("cos ((%d / %d) * 2 * pi) = %lf\n", inum, num_inv, cos);
  }
}