#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#define MAXD 100

typedef struct {
  int num, denom;
} rational;

rational rat_0 = {0, 1};

typedef struct {
  int coeff[MAXD];
  int deg;
} poly_t;

int gcd(int x, int y) {
  x = abs(x);
  y = abs(y);
  int temp;
  while (x != 0) {
    temp = x;
    x = y % x;
    y = temp;
  }
  return y;
}

void r_print(rational r) { printf("(%d/%d) ", r.num, r.denom); }

rational reduce(rational r) {
  int hcf = gcd(r.num, r.denom);
  r.num /= hcf;
  r.denom /= hcf;
  if (r.denom < 0) {
    r.num *= -1;
    r.denom *= -1;
  }
  return r;
}

rational r_add(rational r1, rational r2) {
  rational r3;
  r3.num = r1.num * r2.denom + r2.num * r1.denom;
  r3.denom = r1.denom * r2.denom;
  return reduce(r3);
}

rational r_subtract(rational r1, rational r2) {
  rational r3;
  r3.num = r1.num * r2.denom - r2.num * r1.denom;
  r3.denom = r1.denom * r2.denom;
  return reduce(r3);
}

rational r_multiply(rational r1, rational r2) {
  rational r3;
  r3.num = r1.num * r2.num;
  r3.denom = r1.denom * r2.denom;
  return reduce(r3);
}

rational r_divide(rational r1, rational r2) {
  rational r3;
  r3.num = r1.num * r2.denom;
  r3.denom = r1.denom * r2.num;
  return reduce(r3);
}

rational r_pow(rational x, int n) {
  rational f_x;
  f_x.num = (int)pow(x.num, n);
  f_x.denom = (int)pow(x.denom, n);
  return reduce(f_x);
}

rational eval_poly(poly_t f, rational x) {
  int i;
  rational term, f_x = rat_0, r_coeff;
  for (i = 0; i <= f.deg; i++) {
    r_coeff = (rational){f.coeff[i], 1};
    term = r_multiply(r_coeff, r_pow(x, i));
    f_x = r_add(f_x, term);
  }
  return reduce(f_x);
}

poly_t make_poly(int deg, int coeff[MAXD]) {
  poly_t p;
  int i;
  p.deg = deg;
  for (i = 0; i <= deg; i++) {
    p.coeff[i] = coeff[i];
  }
  return p;
}

void print_poly(poly_t f) {
  int i;
  for (i = f.deg; i > 0; i--) {
    printf("(%d)x^(%d) + ", f.coeff[i], i);
  }
  printf("(%d)", f.coeff[0]);
}

void rational_roots(poly_t f) {
  int num, denom, a_0_abs = abs(f.coeff[0]), a_n_abs = abs(f.coeff[f.deg]);
  rational x, f_x;

  printf("Roots of ");
  print_poly(f);
  printf(" are:\n\t[ ");

  for (num = 1; num <= a_0_abs; num++) {
    if (a_0_abs % num != 0)
      continue;
    for (denom = 1; denom <= a_n_abs; denom++) {
      if (a_n_abs % denom != 0)
        continue;
      x = reduce((rational){num, denom});

      // Condition to discard duplicate roots
      if (x.num != num)
        continue;

      f_x = eval_poly(f, x);
      if (f_x.num == 0) {
        r_print(x);
      }

      x.num *= -1;
      f_x = eval_poly(f, x);
      if (f_x.num == 0) {
        r_print(x);
      }
    }
  }
  printf("]\n\n");
}

int main() {
  poly_t f1, f2, f3, f4;
  rational res;

  int coeff1[4] = {-2, 5, -5, 3};
  f1 = make_poly(3, coeff1);
  rational_roots(f1);

  int coeff2[5] = {24, -10, -113, 14, 120};
  f2 = make_poly(4, coeff2);
  rational_roots(f2);

  int coeff3[5] = {120, 14, -113, -10, 24};
  f3 = make_poly(4, coeff3);
  rational_roots(f3);

  int coeff4[7] = {3, -5, -18, 1, 33, 46, 24};
  f4 = make_poly(6, coeff4);
  rational_roots(f4);
}