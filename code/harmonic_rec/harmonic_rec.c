#include <stdio.h>
int harmonic_rec(double sum, double x, int term) {
  if (sum > x)
    return term;
  return harmonic_rec(sum + 1.0 / (term + 1.0), x, term + 1);
}

int main() {
  double x;
  scanf("%lf", &x);
  printf("Sum of the first %d terms of the harmonic series exceeds %lf.\n",
         harmonic_rec(0, x, 0), x);
  return 0;
}