#include <stdio.h>
#include <stdlib.h>

// Modified from 19MT10017's submission
int **create2d(int n_v) {
  int **s, i, j;
  s = (int **)calloc(n_v, sizeof(int *));
  for (i = 0; i < n_v; i++) {
    s[i] = (int *)calloc(n_v, sizeof(int));
  }
  for (i = 0; i < n_v; i++)
    for (j = 0; j < n_v; j++) s[i][j] = 0;
  return s;
}

void readadjmat(int **adj_mat, int m) {
  int i, j, k;
  // printf("\nEnter the edges in the following way:");
  // printf("\nIf vi directs to vj enter i<space>j\n");
  for (k = 0; k < m; k++) {
    scanf("%d %d", &i, &j);
    adj_mat[i][j] = 1;
  }
}

int **Convert2adjlist(int **adj_mat, int n) {
  int **adj_list, c, i, j;
  adj_list = (int **)calloc(n, sizeof(int *));
  for (i = 0; i < n; i++) {
    c = 0;
    for (j = 0; j < n; j++)
      if (adj_mat[i][j] == 1) c++;
    adj_list[i] = (int *)calloc(c + 1, sizeof(int));
    adj_list[i][c] = -1;
  }
  for (i = 0; i < n; i++) {
    c = 0;
    for (j = 0; j < n; j++) {
      if (adj_mat[i][j] == 1) {
        adj_list[i][c] = j;
        c++;
      }
    }
  }
  return adj_list;
}

int main() {
  int **M, **Adjlist, n, i, j, m;
  // printf("\nEnter the no. of vertices:");
  scanf("%d", &n);
  if (n > 0) {
    M = create2d(n);
    // printf("\nEnter the no. of edges:");
    scanf("%d", &m);
    readadjmat(M, m);
    printf("\nAdjacency Matrix:\n");
    for (i = 0; i < n; i++) {
      for (j = 0; j < n; j++) printf("%d ", M[i][j]);
      printf("\n");
    }
    Adjlist = Convert2adjlist(M, n);
    for (i = 0; i < n; i++) free(M[i]);
    free(M);
    printf("\nAdjacency List:\n");
    for (i = 0; i < n; i++) {
      printf("%d->{", i);
      for (j = 0; Adjlist[i][j] != -1; j++) {
        printf("%d", Adjlist[i][j]);
        if (Adjlist[i][j + 1] != -1) printf(", ");
      }
      // if (j == 0) printf("NULL");
      printf("}\n");
    }
    for (i = 0; i < n; i++) free(Adjlist[i]);
    free(Adjlist);
  }
  if (n == 0) printf("\nVertices should be greater than 0");
  return 0;
}