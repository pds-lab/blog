#include <math.h>
#include <stdio.h>
#include <stdlib.h>

double min(double a, double b) { return (a < b) ? a : b; }

double f(double x, double c0, double c1, double c2, double c3) {
  return c0 + c1 * x + c2 * pow(x, 2) + c3 * pow(x, 3);
}

int cbracket(double lo, double hi, double c0, double c1, double c2, double c3) {
  return (f(lo, c0, c1, c2, c3) * f(hi, c0, c1, c2, c3) <= 0) ? 1 : 0;
}

double root_bisection(double lo, double hi, double eps, double c0, double c1,
                      double c2, double c3) {
  double f_lo, f_hi, mid;
  f_lo = f(lo, c0, c1, c2, c3);
  f_hi = f(hi, c0, c1, c2, c3);
  if (f_lo == 0)
    return lo;
  if (f_hi == 0)
    return hi;
  mid = (hi + lo) / 2;
  if (hi - lo < eps)
    return mid;
  if (cbracket(lo, mid, c0, c1, c2, c3))
    return root_bisection(lo, mid, eps, c0, c1, c2, c3);
  return root_bisection(mid, hi, eps, c0, c1, c2, c3);
}

int main() {
  double c0, c1, c2, c3, start, end, eps, h, lo, hi, root;
  int found = 0;
  scanf("%lf %lf %lf %lf %lf %lf %lf %lf", &c0, &c1, &c2, &c3, &start, &end, &h,
        &eps);
  for (lo = start; lo < end; lo += h) {
    hi = min(lo + h, end);
    if (cbracket(lo, hi, c0, c1, c2, c3)) {
      root = root_bisection(lo, hi, eps, c0, c1, c2, c3);
      found = 1;
    }
  }
  if (found)
    printf("A real root found at %lf.\n", root);
  else
    printf("No real roots could be found within the interval [%lf, %lf] using "
           "root-bisection method of resolution %lf.\n",
           start, end, h);
}