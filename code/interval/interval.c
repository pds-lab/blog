#include <stdio.h>
#define MAXSIZE 1000
int min(int x, int y) { return (x < y) ? x : y; }
int max(int x, int y) { return (x > y) ? x : y; }

int interval(int start1, int end1, int start2, int end2) {
  return max(0, min(end1, end2) - max(start1, start2));
}

int main() {
  int i, j, size, max_i = 0, max_j = 1, max_width = 0, curr_width;
  int start[MAXSIZE];
  int end[MAXSIZE];
  scanf("%d", &size);
  for (i = 0; i < size; i++) {
    scanf("%d %d", &start[i], &end[i]);
  }
  if (size < 2) {
    printf("There should be atleast 2 intervals for finding intersection.\n");
    return -1;
  }
  for (i = 0; i < size; i++) {
    for (j = i + 1; j < size; j++) {
      curr_width = interval(start[i], end[i], start[j], end[j]);
      if (curr_width > max_width) {
        max_i = i;
        max_j = j;
        max_width = curr_width;
      }
    }
  }
  printf("Intervals %d [%d, %d] and %d [%d, %d] have the widest intersection "
         "of width %d.\n",
         max_i, start[max_i], end[max_i], max_j, start[max_j], end[max_j],
         max_width);
}