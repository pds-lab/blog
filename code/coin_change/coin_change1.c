#include <limits.h>
#include <stdio.h>

#define MAXSIZE 100
#define MAXCOST 10000
#define INF MAXCOST + 1

int min(int x, int y) { return (x < y) ? x : y; }

int min_change(int types[MAXSIZE], int size, int idx, int cost) {
  if (cost == 0)
    return 0;
  if ((cost < 0) || (idx == size))
    return INF;
  int min_change_incl, min_change_excl;
  min_change_incl = 1 + min_change(types, size, idx, cost - types[idx]);
  min_change_excl = min_change(types, size, idx + 1, cost);
  return min(min_change_incl, min_change_excl);
}

int num_change(int types[MAXSIZE], int size, int idx, int cost) {
  if (cost == 0)
    return 1;
  if ((cost < 0) || (idx == size))
    return 0;
  int num_change_incl, num_change_excl;
  num_change_incl = num_change(types, size, idx, cost - types[idx]);
  num_change_excl = num_change(types, size, idx + 1, cost);
  return num_change_incl + num_change_excl;
}

int num_change_print(int types[MAXSIZE], int size, int idx, int cost,
                     int change[MAXCOST], int change_idx) {
  if (cost == 0) {
    for (int i = 0; i < change_idx; i++) {
      printf("%2d ", change[i]);
    }
    printf("\n");
    return 1;
  }
  if ((cost < 0) || (idx == size))
    return 0;
  int num_change_incl, num_change_excl;

  change[change_idx] = types[idx];
  num_change_incl = num_change_print(types, size, idx, cost - types[idx],
                                     change, change_idx + 1);

  num_change_excl =
      num_change_print(types, size, idx + 1, cost, change, change_idx);
  return num_change_incl + num_change_excl;
}

int main() {
  int types[MAXSIZE], change[MAXCOST];
  int i, size_types, cost, min_change_val, num_change_val;
  scanf("%d", &size_types);
  if ((size_types < 1) || (size_types > MAXSIZE)) {
    printf("size_types should be in the range [1, %d]\n", MAXSIZE);
    return -1;
  }
  types[0] = 1;
  for (i = 1; i < size_types; i++) {
    scanf("%d", types + i); // or scanf("%d", &types[i]);
  }
  scanf("%d", &cost);
  min_change_val = min_change(types, size_types, 0, cost);
  printf("Minimum number of coins required to sum to %d = %d\n", cost,
         min_change_val);
  num_change_val = num_change(types, size_types, 0, cost);
  printf("Number of possible ways to get a change of %d = %d\n", cost,
         num_change_val);
  num_change_val = num_change_print(types, size_types, 0, cost, change, 0);
  return 0;
}