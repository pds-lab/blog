#include <stdio.h>

#define MAXSIZE 100
#define MAXCOST 10000
#define INF MAXCOST + 1

int min(int x, int y) { return (x < y) ? x : y; }

void min_change(int types[MAXSIZE], int size, int idx, int cost,
                int curr_change_val, int *min_change_val) {
  if (cost == 0) {
    *min_change_val = min(*min_change_val, curr_change_val);
    return;
  }
  if ((cost < 0) || (idx == size))
    return;
  min_change(types, size, idx, cost - types[idx], curr_change_val + 1,
             min_change_val);
  min_change(types, size, idx + 1, cost, curr_change_val, min_change_val);
}

void num_change(int types[MAXSIZE], int size, int idx, int cost,
                int *num_change_val) {
  if (cost == 0) {
    *num_change_val += 1;
    return;
  }
  if ((cost < 0) || (idx == size))
    return;
  num_change(types, size, idx, cost - types[idx], num_change_val);
  num_change(types, size, idx + 1, cost, num_change_val);
}

void num_change_print(int types[MAXSIZE], int size, int idx, int cost,
                      int *num_change_val, int change[MAXCOST],
                      int change_idx) {
  if (cost == 0) {
    for (int i = 0; i < change_idx; i++) {
      printf("%2d ", change[i]);
    }
    printf("\n");
    *num_change_val += 1;
    return;
  }
  if ((cost < 0) || (idx == size))
    return;

  change[change_idx] = types[idx];
  num_change_print(types, size, idx, cost - types[idx], num_change_val, change,
                   change_idx + 1);

  num_change_print(types, size, idx + 1, cost, num_change_val, change,
                   change_idx);
}

int main() {
  int types[MAXSIZE], change[MAXCOST];
  int i, size_types, cost, min_change_val = MAXCOST, num_change_val;
  scanf("%d", &size_types);
  if ((size_types < 1) || (size_types > MAXSIZE)) {
    printf("size_types should be in the range [1, %d]\n", MAXSIZE);
    return -1;
  }
  types[0] = 1;
  for (i = 1; i < size_types; i++) {
    scanf("%d", types + i); // or scanf("%d", &types[i]);
  }
  scanf("%d", &cost);
  min_change(types, size_types, 0, cost, 0, &min_change_val);
  printf("Minimum number of coins required to sum to %d = %d\n", cost,
         min_change_val);
  num_change(types, size_types, 0, cost, &num_change_val);
  printf("Number of possible ways to get a change of %d = %d\n", cost,
         num_change_val);
  num_change_print(types, size_types, 0, cost, &num_change_val, change, 0);
  return 0;
}