#include <math.h>
#include <stdio.h>

float origin_distance(int x, int y) { return sqrt(x * x + y * y); }
void line_equation(int x1, int y1, int x2, int y2) {
  float m, c;
  if (x2 == x1) {
    printf("Equation of line : x = %d", x1);
  } else {
    m = ((float)(y2 - y1)) / (x2 - x1);
    c = y1 - m * x1;
    printf("Equation of line : y = %fx + %f\n", m, c);
  }
}
int main() {
  int n, i, x, y, minx, miny, maxx, maxy;
  float mind = -1, maxd = -1, curd;
  scanf("%d", &n);
  for (i = 1; i <= n; i++) {
    scanf("%d %d", &x, &y);
    if (mind == -1) {
      mind = origin_distance(x, y);
      minx = x;
      miny = y;
      maxd = mind;
      maxx = x;
      maxy = y;
    }
    curd = origin_distance(x, y);
    if (curd < mind) {
      mind = curd;
      minx = x;
      miny = y;
    }
    if (curd > maxd) {
      maxd = curd;
      maxx = x;
      maxy = y;
    }
  }
  printf("Closest point = (%d, %d)\n", minx, miny);
  printf("Furthest point = (%d, %d)\n", maxx, maxy);
  line_equation(minx, miny, maxx, maxy);
  return 0;
}