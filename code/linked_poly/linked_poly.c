#include <math.h>
#include <stdio.h>
#include <stdlib.h>

typedef struct node {
  int exp;
  float coeff;
  struct node *next;
} poly;

poly *createTerm(int exp, float coeff) {
  poly *term = (poly *)malloc(sizeof(poly));
  term->exp = exp;
  term->coeff = coeff;
  term->next = NULL;
  return term;
}

poly *insert(poly *head, int exp, float coeff) {
  if (fabs(coeff - 0) < 1e-6) return head;
  poly *term = createTerm(exp, coeff);
  poly *curr = head;
  while (curr->next != NULL && curr->next->exp > exp) {
    curr = curr->next;
  }
  if (curr->next && curr->next->exp == exp) {
    curr->next->coeff += coeff;
  } else {
    term->next = curr->next;
    curr->next = term;
  }
  return head;
}

poly *readpoly(int numterms) {
  int iterm, exp;
  float coeff;
  poly *head = createTerm(0, 0);  // Head
  // printf("Enter %d (exp, coeff) pairs :\n", numterms);
  for (iterm = 0; iterm < numterms; iterm++) {
    scanf("%d %f", &exp, &coeff);
    head = insert(head, exp, coeff);
  }
  return head;
}

void printpoly(poly *head) {
  poly *curr;
  for (curr = head->next; curr; curr = curr->next) {
    printf("(%10f)x^(%2d)", curr->coeff, curr->exp);
    if (curr->next) printf(" + ");
  }
  printf("\n");
}

float evalpoly(poly *head, float x) {
  poly *curr;
  float y = 0;
  for (curr = head->next; curr; curr = curr->next) {
    y += curr->coeff * pow(x, curr->exp);
  }
  return y;
}
// More code but faster implementation of addpoly
poly *addpoly(poly *head1, poly *head2) {
  poly *head = createTerm(0, 0);
  poly *c1, *c2, *curr = head;
  for (c1 = head1->next, c2 = head2->next; c1 && c2; curr = curr->next) {
    if (c1->exp == c2->exp) {
      curr->next = createTerm(c1->exp, c1->coeff + c2->coeff);
      c1 = c1->next;
      c2 = c2->next;
    } else if (c1->exp > c2->exp) {
      curr->next = createTerm(c1->exp, c1->coeff);
      c1 = c1->next;
    } else {
      curr->next = createTerm(c2->exp, c2->coeff);
      c2 = c2->next;
    }
  }
  if (c1) curr->next = c1;
  if (c2) curr->next = c2;
  return head;
}
// Less code but slower implementation of addpoly
poly *addpoly2(poly *head1, poly *head2) {
  poly *head = createTerm(0, 0);
  poly *c1, *c2, *curr = head;
  for (c1 = head1->next; c1 != NULL; c1 = c1->next) {
    head = insert(head, c1->exp, c1->coeff);
  }
  for (c2 = head2->next; c2 != NULL; c2 = c2->next) {
    head = insert(head, c2->exp, c2->coeff);
  }
  return head;
}

poly *mulpoly(poly *head1, poly *head2) {
  poly *head = createTerm(0, 0);
  poly *c1, *c2, *curr = head;
  for (c1 = head1->next; c1 != NULL; c1 = c1->next) {
    for (c2 = head2->next; c2 != NULL; c2 = c2->next) {
      head = insert(head, c1->exp + c2->exp, c1->coeff * c2->coeff);
    }
  }
  return head;
}

int main() {
  int numterms = 0;
  float x;
  poly *poly1, *poly2;
  // printf("+++ f1:\n");
  // printf("Enter number of terms : ");
  scanf("%d", &numterms);
  poly1 = readpoly(numterms);
  // printf("+++ f2:\n");
  // printf("Enter number of terms : ");
  scanf("%d", &numterms);
  poly2 = readpoly(numterms);

  printf("The two polynomials are :\n");
  printf("f1(x) = ");
  printpoly(poly1);
  printf("f2(x) = ");
  printpoly(poly2);

  // printf("Enter the value of x : ");
  scanf("%10f", &x);
  printf("f1(%10f) = %-20f\n", x, evalpoly(poly1, x));
  printf("f2(%10f) = %-20f\n", x, evalpoly(poly2, x));

  printf("f1(x) + f2(x) = ");
  printpoly(addpoly(poly1, poly2));

  printf("f1(x) * f2(x) = ");
  printpoly(mulpoly(poly1, poly2));
}