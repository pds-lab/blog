#include <stdio.h>

int main() {
  int num, sum, rev;
  scanf("%d", &num);
  if (num < 100 || num > 999)
    return -1;
  // One's digit -> (num % 10)
  // Ten's digit -> ((num / 10) % 10)
  // Hundredth's digit -> (num / 100)
  sum = (num % 10) + ((num / 10) % 10) + (num / 100);
  rev = (num % 10) * 100 + ((num / 10) % 10) * 10 + (num / 100);
  printf("Number = %d\n", num);
  printf("Sum of the digits = %d\n", sum);
  printf("Reverse of %d = %d\n", num, rev);
  return 0;
}