#include <stdio.h>

int main() {
  int n, cnt1 = 0, cnt2 = 0, cnt3 = 0, cnt4 = 0, cnt5 = 0, f0 = 0, f1 = 1, f2;
  scanf("%d", &n);
  if (n >= 0) {
    printf("%d ", f0);
    cnt1 += 1;
  }
  if (n >= 1) {
    printf("%d ", f1);
    cnt1 += 1;
    while (1) {
      f2 = f0 + f1;
      if (f2 > n)
        break;
      if (f2 < 10)
        cnt1++;
      else if (f2 < 1e2)
        cnt2++;
      else if (f2 < 1e3)
        cnt3++;
      else if (f2 < 1e4)
        cnt4++;
      else if (f2 < 1e5)
        cnt5++;
      printf("%d ", f2);
      f0 = f1;
      f1 = f2;
    }
    printf("\n+++ Fibonacci numbers with:\n");
    printf("\t1 digit  : %d\n", cnt1);
    printf("\t2 digits : %d\n", cnt2);
    printf("\t3 digits : %d\n", cnt3);
    printf("\t4 digits : %d\n", cnt4);
    printf("\t5 digits : %d\n", cnt5);
  }
}