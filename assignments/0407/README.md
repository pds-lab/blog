---
title: Week 13 - Apr 21
sidebar: auto
---

## Words IO

### Problem

For problem statement check moodle

### Sample I/O

#### Input 1
<<< @/code/linked_poly/in1

#### Output 1
<<< @/code/linked_poly/out1

#### Input 2
<<< @/code/linked_poly/in2

#### Output 2
<<< @/code/linked_poly/out2

### Sample Solution

<<< @/code/linked_poly/linked_poly.c
