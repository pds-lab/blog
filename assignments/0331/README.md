---
title: Week 11 - Mar 31
sidebar: auto
---

## Words IO

### Problem

For problem statement check moodle

### Sample I/O

#### Input File Content
<<< @/code/words.io/input.txt

#### Output File Content
<<< @/code/words.io/output.txt

### Sample Solution

<<< @/code/words.io/words.io.c
