---
title: Week 6 - Feb 11 (LabTest 1)
sidebar: auto
---

## ExpRec (Even Q1)

### Problem

The value of Exponential function can be calculated using Taylor Series. $e^x = 1 + x/1! + x^2/2! + x^3/3! + ...$ <br/>
Write a recursive function to compute the sum of the series to $n$ terms. 

### Sample I/O

#### Input 1
<<< @/code/exp_rec/in1

#### Output 1
<<< @/code/exp_rec/out1

#### Input 2
<<< @/code/exp_rec/in2

#### Output 2
<<< @/code/exp_rec/out2

#### Input 3
<<< @/code/exp_rec/in3

#### Output 3
<<< @/code/exp_rec/out3

### Sample Solution

<<< @/code/exp_rec/exp_rec.c

## Interval (Even Q2)

### Problem


1. Write a function interval 0 that takes as input the endpoints of two intervals (A1, B1) and (A2, B2) and returns the width of the intersecting interval.<br/> Example: 
    1. Given the intervals `[2, 6]` and `[4, 7]` it returns 2 (since the intersecting interval is `[4, 6]`)
    2. Given the intervals `[5, 16]` and `[1, 4]` it returns 0 (since the intersecting interval is empty)

2. Suppose `A[]` and `B[]` store the left and right endpoints of N intervals. Write a program which does the following:
    1. Read the number of intervals N.
    2. Read the end points of N intervals and store them in A and B.
    3. Find a pair of intervals whose intersecting interval is widest. Print the indices of this interval i and j and the width of the intersecting interval.<br/>
  Example: Suppose the intervals are: `[1, 5]`, `[7, 9]`, `[3, 8]`, `[8, 12]`. The intervals with widest intersection are 0 and 2. The width is 2.

### Sample I/O

#### Input 1
<<< @/code/interval/in1

#### Output 1
<<< @/code/interval/out1

#### Input 2
<<< @/code/interval/in2

#### Output 2
<<< @/code/interval/out2

#### Input 3
<<< @/code/interval/in3

#### Output 3
<<< @/code/interval/out3

### Sample Solution

<<< @/code/interval/interval.c

## HarmonicRec (Odd Q1)

### Problem

Consider the Harmonic series: $1 + 1/2 + 1/3 + 1/4 + 1/5 + ...$<br/>
Read a number `X`. Write a recursive function to find the minimum number of terms that may be summed of the above series so that the sum exceeds `X`.

### Sample I/O

#### Input 1
<<< @/code/harmonic_rec/in1

#### Output 1
<<< @/code/harmonic_rec/out1

#### Input 2
<<< @/code/harmonic_rec/in2

#### Output 2
<<< @/code/harmonic_rec/out2

#### Input 3
<<< @/code/harmonic_rec/in3

#### Output 3
<<< @/code/harmonic_rec/out3


### Sample Solution

<<< @/code/harmonic_rec/harmonic_rec.c

## ClosestPair (Odd Q2)

### Problem

1. Write a function `distance()` that computes and returns the **Euclidean distance** between two points `(x1 , y1)` and `(x2, y2)`.
2. Suppose the `x` and `y` coordinates of n points are stored in two arrays `X` and `Y` such that `X[i]` and `Y[i]` are the coordinates of the ith point. Write a program which does the following:<br/>
    1. Read the number of points `n`.
    2. Read the coordinates of `n` points and stores them in `X` and `Y`.
    3. Find the pair of points which are closest to each other by calling the function `distance()`. Print the coordinates of the two points and the distance between them. 


### Sample I/O

#### Input 1
<<< @/code/closest_pair/in1

#### Output 1
<<< @/code/closest_pair/out1

#### Input 2
<<< @/code/closest_pair/in2

#### Output 2
<<< @/code/closest_pair/out2

#### Input 3
<<< @/code/closest_pair/in3

#### Output 3
<<< @/code/closest_pair/out3

### Sample Solution

<<< @/code/closest_pair/closest_pair.c