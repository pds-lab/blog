---
title: Week 9 - Mar 17
sidebar: auto
---

## Anagrams

### Problem

In this program you will read a list of words. Each word has lower case characters only
and does not contain any blank spaces. You may assume that there are a maximum of
`100 words` and each word has at the most `15 characters`. You need to find all sets of
words which are anagrams and print the anagram sets.<br/>
The following definitions and functions must be used in your program.

* Define a struct:

```c
#define MAXC 15
#define MAXW 100
typedef struct {
  char word[MAXC];
  char sorted[MAXC];
} word_t;
```
The structure is meant to store a word and the word formed by sorting the characters.

Write the following functions:

1. `int readwords(word_t wordlist[])`<br/>
This function reads words from the user and put them in the array of structures
wordlist. The read words are put in the “word” field. The function returns the
number of words read.

2. `void sortword(word_t *pword)`<br/>
This function takes a parameter pword which is a pointer to a structure of type
`word_t` . It creates a string comprising of the sorted list of characters of
`pword->word` and puts this in `pword->sorted` . The string `pword->word` is not
changed.<br/>
**Example: If `word` is `characters`, sorted will be `aaccehrrst`**

3. `int is_anagram(word_t *pword1, word_t *pword2)`<br/>
This function takes as input the pointer to two structures of type `word_t`
It will return `1` if `pword1->word` is an anagram of `pword2->word` and `0`
otherwise.<br/>
A word `w1` is an anagram of another word `w2` if `w2` can be formed by rearranging
the letters of `w1`, using all the original letters exactly once.<br/>
**Example: `dessert` and `stressed` are anagrams.**

4. `void sortwordlist(word_t wordlist[], int numwords)`<br/>
This function sorts the array elements in wordlist based on the `sorted` field.<br/>
**Example: If `wordlist` contains**
```
{ {“ant”, “ant”},
 {“silent”, “eilnst”},
 {“list”, “ilst”},
 {“listen”, “eilnst”},
 {“tan”, “ant”},
 {“enlist”, “einlst”}}
```
**After sorting the `wordlist` may contain**
```
{ {“ant”, “ant”},
 {“tan”, “ant”},
 {“silent”, “eilnst”},
 {“listen”, “eilnst”},
 {“enlist”, “einlst”},
 {“list”, “ilst”} } 
```

5. `void printanagrams(word_t wordlist[], int numwords)`<br/>
This function takes the array of structures and prints all set of anagrams.
    * It first calls `sortwordlist()`.
    * Then it calls `is_anagram()` on successive array elements and prints the sets of anagrams.

**Example:**
For the example in **4** it will print
```
ant tan
silent listen enlist
list
```

6. `int main()`
    * Declare `word_t wordlist[MAXW]`.
    * Call `readwords()` to read words into the `word` field of wordlist array.
    * Calls `sortword()` on the pointers to all the elements of the array.
    * Call `printanagrams()` to print the anagrams.

::: tip Note
You may use standard library functions on strings, such as `strcpy()`, `strlen()` and `strcmp()`
:::

### Sample I/O

#### Input 1
<<< @/code/anagrams/in1

#### Output 1
<<< @/code/anagrams/out1

### Sample Solution

<<< @/code/anagrams/anagrams.c
