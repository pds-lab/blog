---
title: Week 4 - Jan 28
sidebar: auto
---

## Line

### Problem

1. Write a function which takes as parameters the x and y co-ordinates of a point, and returns the Euclidean distance of the point from the origin. `float origin_distance (int x, int y);`
2. Write a function that takes as parameters the x and y co-ordinates of two points, and prints the equation of a line that passes through these two points. `void line_equation(int x1, int x2, int y1, int y2);`
3. In the main ( ) function read N, and read N points. Print the point that is closest to the origin, and the one that is the furthest from the origin. Find the equation of a straight line that passes through these two points. Your program must call the functions defined.
4. Submit the file `line.c`.

### Sample I/O

#### Input 1
<<< @/code/line/in1

#### Output 1
<<< @/code/line/out1

#### Input 2
<<< @/code/line/in2

#### Output 2
<<< @/code/line/out2

### Sample Solution

<<< @/code/line/line.c

## Root

### Problem

The bisection method is a root-finding method that applies to any continuous functions for which one knows two values with opposite signs. The method consists of repeatedly bisecting the interval defined by these values and then selecting the subinterval in which the function changes sign, and therefore must contain a root.
The bisection method of finding the root proceeds as follows in each iteration:

1. Calculate `r`, the midpoint of the interval,
2. Calculate the function value at the midpoint, `f(r)`
3. If `f(r) = 0`, then r is the root.
4. If `|b − a|` is very small, then also we can take r as the root.
5. Examine the sign of `f(r)` and replace either `(a, f(a))` or `(b, f(b))` with `(r, f(r))` so that there is a zero crossing within the new interval.

Write the following functions with the specifications mentioned.

1. Function `double fun1 (double x)` takes a real number x as argument and returns the value of `f(x)`.
2. Function `int cbracket (double a, double b)` takes two real numbers a and b as arguments and returns 1 if at least one real root of `f(x)` lies between a and b, and 0 otherwise
3. Function `double rootb (double a, double b, double eps, int Nmax)` takes three real numbers `a`, `b`, `eps` and an integer `Nmax` as arguments. This function returns the root of `f(x) = 0` using bisection method. If the number of iterations is more than `Nmax` then the function terminates with appropriate message.
4. Write a C program using the above functions. This program accepts `a`, `b`, `eps` and `Nmax` from the keyboard and prints out the root (if any).

Run the program to find a real root of the function $x^{3}-x-11=0$ .

### Sample I/O

#### Input 1
<<< @/code/root/in1

#### Output 1
<<< @/code/root/out1

#### Input 2
<<< @/code/root/in2

#### Output 2
<<< @/code/root/out2

#### Input 3
<<< @/code/root/in3

#### Output 3
<<< @/code/root/out3

### Sample Solution

<<< @/code/root/root.c
