---
title: Week 3 - Jan 21
sidebar: auto
---

## Fibonacci

### Problem

The Fibonacci numbers, commonly denoted Fn form a sequence, called the Fibonacci sequence, such that each number is the sum of the two preceding ones starting from 0 and 1.
```
fib(0) = 0
fib(1) = 1
fib(n) = fib(n - 1) + fib(n - 2)
```
Write a C program that does the following:
1. Take an integer N as input through the keyboard
2. Print all the Fibonacci numbers which are less than or equal to N.
3. Print the count of the Fibonacci numbers generated which are 1 digit, 2 digits, 3 digits, 4 digits and 5 digits.
4. Submit the file `fibonacci.c`.

### Sample I/O

#### Input 1
<<< @/code/fibonacci/in1

#### Output 1
<<< @/code/fibonacci/out1

#### Input 2
<<< @/code/fibonacci/in2

#### Output 2
<<< @/code/fibonacci/out2

### Sample Solution

<<< @/code/fibonacci/fibonacci.c



## Cosine

### Problem

The following series can be summed to get the cosine of a number.
```
cos (x) = 1 - x^2/2! + x^4/4! -  ...
```
Write a C program that does the following:
1. Ask the user for an integer `num_intervals` and read the value.
2. Ask the user for the resolution res which is a floating point value less than `0.001`
3. While computing the cosine function sum the terms until the difference between two successive sums is less than res
4. Print out the value of the cosine function inside the interval `[0, 2 pi]` at `(num_intervals + 1)` equally spaced points.
5. Submit the file `cosine.c`.

### Sample I/O

#### Input 1
<<< @/code/cosine/in1

#### Output 1
<<< @/code/cosine/out1

#### Input 2
<<< @/code/cosine/in2

#### Output 2
<<< @/code/cosine/out2

### Sample Solution

<<< @/code/cosine/cosine.c
