---
title: Week 10 - Mar 23
sidebar: auto
---

## Directed Graph

### Problem

* A directed graph comprises of a set of vertices `V` and a set of edges `E`. An edge is directed from one vertex `v_i` to another vertex `v_j`.
    * A graph of `n` vertices may be represented by an adjacency matrix which is a 2-dimensional array `M`. `M[i][j] = 1` if there is an edge from `v_i` to `v_j` and `M[i][j] = 0` otherwise.
    * Alternatively, the above graph may be represented as an adjacency list for which we adopt the following protocol: the adjacency list representation comprises of an array of `n` pointers adjlist such that `adjlist[i]` points to an integer array which contains `n_i` elements if there are `n_i` edges from `v_i` . This array contains the list of the vertices to which there is an edge from `v_i`. Further assume that this array is sorted.

1. Write a function `convert2adjlist ()` that takes as input an adjacency representation of a graph, and returns the array of pointers representing the adjacency list of the graph. `int ** Convert2adjlist (int **M, int n)`
2. Write a function `create2d ()` to create a 2d array. This function returns the 2d array created. `int ** create2d (int n)`
3. Write the the function `readadjmat ()` that reads the edges from the user and populates `M` so that it represents the adjacency matrix of the graph.  `void readadjmat (int ** Adjmat, int m) ;`
4. Write a `main ()` function that does the following:
    1. Read the number of vertices from the user in variable `n`.
    2. Dynamically allocate memory to store a `n x n` 2-dimensional array in `M` by calling function `create2d ()`
    3. Read the number of edges, `m`, from the user. Call the function `readadjmat ()` that reads the edges from the user and populates M so that it represents the adjacency matrix of the graph.
    4. Print the adjacency matrix `M`.
    5. Call `convert2adjlist ()` to convert `M` to the adjacency list representation described above.
    6. Free memory allocated for `M`
    7. Print the adjacency list
    8. Free memory associated with the adjacency list.

### Sample I/O

#### Input 1
<<< @/code/directed_graph/in1

#### Output 1
<<< @/code/directed_graph/out1

#### Input 2
<<< @/code/directed_graph/in2

#### Output 2
<<< @/code/directed_graph/out2

### Sample Solution

<<< @/code/directed_graph/directed_graph.c
