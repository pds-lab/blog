---
title: Week 5 - Feb 4
sidebar: auto
---

## Root2

### Problem

Write a C program to find a root of a given equation. The function f is a polynomial function of degree 3 of the form $c_0 + c_1*x + c2*x^2 + c3 * x^3$

1. Write a recursive C function to find the root of the equation $f(x) = 0$ within a given interval $[lo, hi]$ – $x$ may be considered a root if $|f(x)| < eps$ or $hi - lo < eps$. `float root_bisection (float lo, float hi, float eps)`
2. Write a `main()` function which does the following:
    1. Read `c0`, `c1`, `c2`, `c3` corresponding to the parameters of the function `f()`
    2. Read the range of values of x `(A, B)` within which the root is to be found.
    3. Read `h`, the size of the interval for initial inspection.
    4. Find one i if it exists so that `f(A+i*h)` and `f(A+(i+1)*h)` are of opposite signs. Call the function `root_bisection()` to find a root of the function.

### Sample I/O

#### Input 1
<<< @/code/root2/in1

#### Output 1
<<< @/code/root2/out1

#### Input 2
<<< @/code/root2/in2

#### Output 2
<<< @/code/root2/out2

#### Input 3
<<< @/code/root2/in3

#### Output 3
<<< @/code/root2/out3

### Sample Solution

<<< @/code/root2/root2.c

## NoPrimeArray

### Problem

Write a C program which does the following:

1. Declare an integer array A of dimension `MAXSIZE`
2. Read `n` (the size of the array, less than `MAXSIZE`)
3. Create a random array whose elements are between `0` and `MAX - 1`: 
  ```c
    for (i=0; i<n; i++)
      A[i] = rand() % MAX;
  ```
4. Print the array A
5. Remove all elements of the array which are prime numbers
6. Print the array A (with the remaining elements)

### Sample I/O

#### Input 1
<<< @/code/no_prime_array/in1

#### Output 1
<<< @/code/no_prime_array/out1

#### Input 2
<<< @/code/no_prime_array/in2

#### Output 2
<<< @/code/no_prime_array/out2

#### Input 3
<<< @/code/no_prime_array/in3

#### Output 3
<<< @/code/no_prime_array/out3

### Sample Solution

<<< @/code/no_prime_array/no_prime_array.c
