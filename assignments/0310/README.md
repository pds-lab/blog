---
title: Week 8 - Mar 10
sidebar: auto
---

## RationalPolynomial

### Problem

1. Define a structure `rational` to store the numerator and denominator of a rational number.
    * Write a function that takes a rational number as input and returns its reduced form. A rational number $x/y$ is said to be in the reduced form if the greatest common divisor of $x$ and $y$ is $1$.
    * Write four functions to perform the basic operations with rational numbers: `addition`, `subtraction`, `multiplication` and `division`. These functions should take two rational numbers as input and return the output in the reduced form.

2. Define a structure `polynomial` to store the degree as well as the integer coefficients of the polynomial.
    * Write a function `evalPoly(f, x)` that evaluates a polynomial $f(x)$ at a rational value $x$.
    * Write a function `rationalRoots(f)` that finds out all the rational roots of $f(x)$ and prints. You can use the rational **root theorem (or rational root test).**

::: tip Rational Root Theorem
Suppose $f(x) = a_{n}x^{n} + a_{n - 1}x^{n - 1} + ...+ a_{0}x^{0}$ be a polynomial of degree $n$ such that $a_n$ and $a_0$ are not zero. <br/>
Then, all the rational roots of $f(x)$ are of the form $\dfrac{a}{b}$ or $-\dfrac{a}{b}$,<br/>
where $a$ is a positive integer divisor of $a_0$ and $b$ is a positive integer divisor of $a_n$.
:::

3. Write a main function that declares a polynomial. Report the output of your program for the following polynomial functions:
    * $3x^3 - 5x^2 + 5x - 2$
    * $120x^4 + 14x^3 − 113x^2 − 10x + 24$
    * $24x^4− 10x^3− 113x^2 + 14x + 120$
    * $24x^6+ 46x^5+ 33x^4+ x^3 − 18x^2− 5x + 3$

### Output
<<< @/code/rational_polynomial/out

### Sample Solution

<<< @/code/rational_polynomial/rational_polynomial.c
