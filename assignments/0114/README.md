---
title: Week 2 - Jan 14
sidebar: auto
---

## Number

### Problem

Write a C program which:
1. Takes as input a three digit number `x (100 <= x <= 999)`.
2. Check if the number is three digits. If not, exit.
3. Write an expression to find the sum of digits of the number. Print this sum.
4. Write a second expression to find the number with the digits reversed. Print this number.
5. Submit the file `number.c`.

### Sample I/O

#### Input 1
<<< @/code/number/in1

#### Output 1
<<< @/code/number/out1

#### Input 2
<<< @/code/number/in2

#### Output 2
<<< @/code/number/out2

### Sample Solution

<<< @/code/number/number.c

## Circle

### Problem

Write a C program that does the following:
1. Read the co-ordinates of three points: A=(x1, y1),  B=(x2, y2), and C=(x3, y3).  
2. Print the lengths of the sides AB, BC and AC.                                                        
3. Print the radius of the circle passing through A, B and C.                             
4. Find and print the coordinates of the centre of the above circle.                           
5. Take as input the co-ordinates of another point (x4, y4). Print whether the point lies within the circle, on the cirle or outside the circle.
6. Submit the file `circle.c`.


![eq_0](./eq_0.svg)
![eq_1](./eq_1.svg)
![eq_2](./eq_2.svg)
![eq_3](./eq_3.svg)
![eq_4](./eq_4.svg)
![eq_5](./eq_5.svg)
![eq_6](./eq_6.svg)

<!-- $|A - P| = |B - P| = |C - P|$
$(x_1 - k)^2 + (y_1 - s)^2 = (x_2 - k)^2 + (y_2 - s)^2 = (x_3 - k)^2 + (y_3 - s)^2$
$(x_1^2 + y_1^2) - (x_2^2 + y_2^2) = 2k(x_1 - x_2) + 2s(y_1 - y_2)$
$(x_1^2 + y_1^2) - (x_3^2 + y_3^2) = 2k(x_1 - x_3) + 2s(y_1 - y_3)$
$$\begin{bmatrix} x_{1}-x_{2} & y_{1}-y_{2} \\ x_{1}-x_{3} & y_{1}-y_{3} \end{bmatrix}\begin{bmatrix} k \\ s \end{bmatrix}=\begin{bmatrix} x^{2}_{1}+y^{2}_{1}-x^{2}_{2}-y^{2}_{2} \\ x^{2}_{1}+y^{2}_{1}-x^{2}_{3}-y^{2}_{3} \end{bmatrix}$$

$$\begin{bmatrix} k \\ s \end{bmatrix}=\dfrac {1}{2}\dfrac {1}{\begin{vmatrix} x_{1}-x_{2} & y_{1}-y_{2} \\ x_{1}-x_{3} & y_{1}-y_{3} \end{vmatrix}}\begin{bmatrix} y_{1}-y_{3} & y_{2}-y_{1} \\ x_{3}-x_{1} & x_{1}-x_{2} \end{bmatrix} \begin{bmatrix} x^{2}_{1}+y^{2}_{1}-x^{2}_{2}-y^{2}_{2} \\ x^{2}_{1}+y^{2}_{1}-x^{2}_{3}-y^{2}_{3} \end{bmatrix}$$

$$\begin{bmatrix} k \\ s \end{bmatrix}= \dfrac {1}{2\left\{ \left( x_{1}-x_{2}\right) \left( y_{1}-y_{3}\right) -\left( x_{1}-x_{3}\right) \left( y_{1}-y_{2}\right) \right\} } \begin{bmatrix} \left( y_{1}-y_{3}\right) \left( x^{2}_{1}+y^{2}_{1}-x^{2}_{2}-y^{2}_{2}\right) - \left( y_{1}-y_{2}\right) \left( x^{2}_{1}+y^{2}_{1}-x^{2}_{3}-y^{2}_{3}\right)\\ \left( x_{1}-x_{2}\right) \left( x^{2}_{1}+y^{2}_{1}-x^{2}_{3}-y^{2}_{3}\right) - \left( x_{1}-x_{3}\right) \left( x^{2}_{1}+y^{2}_{1}-x^{2}_{2}-y^{2}_{2}\right) \end{bmatrix}$$ -->

### Sample I/O

#### Input 1
<<< @/code/circle/in1

#### Output 1
<<< @/code/circle/out1

#### Input 2
<<< @/code/circle/in2

#### Output 2
<<< @/code/circle/out2

#### Input 2
<<< @/code/circle/in3

#### Output 2
<<< @/code/circle/out3

### Sample Solution

<<< @/code/circle/circle.c