---
title: Week 7 - Mar 3
sidebar: auto
---

## RemoveBlanks

### Problem

Write a C program to find a root of a given equation. The function f is a polynomial function of degree 3 of the form $c_0 + c_1*x + c2*x^2 + c3 * x^3$

1. Write a function `removeblanks()` that takes a string as input and removes all blank characters from the string. It returns the number of words in the input. A word is defined as a **sequence of characters separated by blank or newline**
2. Write a program that reads all characters in the input (till a newline) into a string. It then calls the above function and prints the number of words in the original string and the string after removal of blanks. 

### Sample I/O

#### Input 1
<<< @/code/remove_blanks/in1

#### Output 1
<<< @/code/remove_blanks/out1

#### Input 2
<<< @/code/remove_blanks/in2

#### Output 2
<<< @/code/remove_blanks/out2

#### Input 3
<<< @/code/remove_blanks/in3

#### Output 3
<<< @/code/remove_blanks/out3

### Sample Solution

<<< @/code/remove_blanks/remove_blanks.c

## CoinChange

### Problem

You are given $n (>=1)$ types of coins $v_0, v_1, v_2 ... v_n$. Assume $v_0 = 1$ and all $v_i$ to be unique. You need to make a coin change for amount or cost $C$.
Also assume the user inputs $v_i (i > 0)$ in ascending order. *(Though this assumption is not exploited in the solution)*<br/>
1. Write a (recursive) function `min_change()` to find the smallest number of coins required to sum to `C` exactly.
2. Write a (recursive) function `ways_to_change()` to find the number of ways to count the total number of ways we can make change of `C`.
3. Modify `ways_to_change()` to print all ways of making change.

*For more detailed information of the problem check out the Assignment PDF in CSE moodle.*

::: warning Alert
The given recursive functions can be implemented both using `void` and `int` return types. Solution using both are given.
The void return type uses **pointers** (and not global variables), so please have a read on pointers before checking that solution out.
:::

### Sample I/O

#### Input 1
<<< @/code/coin_change/in1

#### Output 1
<<< @/code/coin_change/out1

#### Input 2
<<< @/code/coin_change/in2

#### Output 2
<<< @/code/coin_change/out2

#### Input 3
<<< @/code/coin_change/in3

#### Output 3
<<< @/code/coin_change/out3

### Sample Solution

1. **Using int type recursive functions**

<<< @/code/coin_change/coin_change1.c

2. **Using void type recursive functions**

<<< @/code/coin_change/coin_change2.c
