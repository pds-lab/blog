---
title: Assignments
sidebar: auto
---

1. Week 1
2. [Week2 : Jan 14](./0114/)
3. [Week3 : Jan 21](./0121/)
4. [Week4 : Jan 28](./0128/)
5. [Week5 : Feb 4](./0204/)
6. [Week6 : Feb 11 (LabTest 1)](./0211)
7. [Week7 : Mar 3](./0303)
8. [Week8 : Mar 10](./0310)
9. [Week9 : Mar 17](./0317)
10. [Week10 : Mar 23](./0323)
11. [Week11 : Mar 31](./0331)
11. [Week12 : Apr 07](./0407)
11. [Week13 : Apr 21](./0421)
