---
title: Tips and Tricks
sidebar: auto
---

[[toc]]

## Code Editor

You can use several modern IDEs for C programming, including **Sublime Text**, **Clion** and **Visual Studio Code (VSCode)**.
**VSCode** is my personal favourite and I've included the installation steps below:

### Install on Linux PC without root access

To install [VSCode](https://code.visualstudio.com/) on a PC without root access (for eg, Lab PCs) open up a terminal and paste:

```bash
sh -c "$(wget -O- https://gitlab.com/pds-lab/vscode-config/raw/master/install.sh)"
```

Now, to open VSCode on the directory where you want to code:
``` bash
code .
```

### Install on your computer

Go to [the official website](https://code.visualstudio.com/), download the software and follow the next steps.

### Recommended extensions of VSCode

1. [C/C++](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)
2. [Bracket Pair Colorizer 2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)
3. [indent-rainbow](https://marketplace.visualstudio.com/items?itemName=oderwat.indent-rainbow)
4. [Atom Keymap](https://marketplace.visualstudio.com/items?itemName=ms-vscode.atom-keybindings)

::: tip
1. *Emacs* is an advanced text-editor (and so is *vim*). However, it requires much practice and configuration for advanced usage. It's better to use a modern IDE.
2. Debugging programs on VSCode is easy. You should learn it over time.
:::

::: tip Using the terminal
[Article](https://maker.pro/linux/tutorial/basic-linux-commands-for-beginners)
[Video](https://www.youtube.com/watch?v=1ROOqJ9yNT0)
:::

::: tip Debugging C in VSCode
[Article](https://dev.to/jerrygoyal/run-debug-get-intellisense-for-c-c-in-vscode-4e0o)
[Video](https://www.youtube.com/watch?v=X2tM21nmzfk)
:::

## Working with Sample I/O

1. It's not necessary to `printf("Input X")` before taking the input as `scanf("%d", &x)`. You should avoid this.
2. It's recommended to take input from files instead of typing it everytime you run a program. It saves plenty of time.
3. Here's a workflow

Suppose you want to run sqrt.c which is in `~/Code/` (`~` is the **HOME** directory).

``` bash
cd ~/Code
gcc sqrt.c -lm
```
Remember you are in the directory `~/Code`.<br/>
Save the input in a file, say `inp`. You want the output not in the terminal but in a file, say `out`.
To run the executable `a.out`, reading input from `inp` and writing output to `out`:
``` bash
./a.out < inp > out
```
The generic command is:
``` bash
[path to excutable] < [path to input file] > [path to output file]
```